<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Rozecart</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="freelancer.min.css">
    <link rel="stylesheet" type="text/css" href="animate.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="jquery-3.2.1.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <style type="text/css">
     .section1{
      padding-left: 0px;
      padding-right: 0px;
    }
    .nav>li>a:focus, .nav>li>a:hover {
      text-decoration: none;
      background-color: rgba(238, 238, 238, 0.03);
    }
    </style>
  </head>
  <body id="main">
<!-- navigation bar of page -->
    <nav class="navbar navbar-fixed-top head">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"  aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span  class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
          <a class="navbar-brand page-scroll" href="#main" id="roze" style="color: #fff; ">Rozecart</a>
        </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="float: right;">
            <ul class="nav navbar-nav">
              <li><a href="#main" id="nav1" class="lists page-scroll">Home</a></li>
              <li><a href="#about" id="nav2" class="lists page-scroll">About</a></li>
              <li><a href="#service" id="nav3" class="lists page-scroll">Service</a></li>
              <li><a href="#team" id="nav4" class="lists page-scroll">Team</a></li>
              <li><a href="#work" id="nav5" class="lists page-scroll">Work</a></li>
              <li><a href="#contact" id="nav6" class="lists page-scroll">Contact</a></li>
            </ul>
          </div>
      </div>
    </nav>
      <!-- Home page of rozecart -->
    <div class="container-fluid section1">
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
          <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
          </ol>
        <!-- Wrapper for slides -->
        <div class="carousel-inner">  
          <div class="item active parallax">
            <img src="./image/wu-yi-152057.jpg" class="" class="img-responsive" alt="Los Angeles">
             <div class="carousel-caption">
              <h1 style="position: center; vertical-align: center; color: black;">New York</h1>
              <p>We love the Big Apple!</p>
            </div>
          </div>
          <div class="item parallax">
            <img class="img-responsive"  class="" src="./image/4.jpg" alt="Chicago">
           <div class="carousel-caption cap">
            <h2>Slide 3</h2>
        </div>
          </div>
          <div class="item parallax">
            <img src="./image/3.jpg"  class="" alt="New york" class="img-responsive">
          </div>
        </div>
          <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" "></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" ></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    <br>
      <!-- about the  rozecart -->
    <div class="box"  id="about">
      <div class="container-fluid">
          <div class="about">
            <div class="row">
              <h3 class="team">About</h3>
                <span class="line" > </span>
                      <br><hr>
              <div class="col-md-5 col-md-offset-1" style="line-height: 30px;">
                <div class="container-fluid">
                    <p class="text">
                      AT CYBEROZE WE BRING THE BEST PEOPLE AND THE IDEAS TOGETHER TO BUILD THE DREAM OF OUR CUSTOMERS AND ASSURE SEAMLESS SERVICE. OUR PECULIARITY HAS BEEN IDENTIFIED WITH US SINCE FOUNDATION IS THAT OF AN INNOVATOR. WE HAVE INNOVATIVE PROFESSIONALS OF IT INDUSTRIES WHO ALLOWS YOU TO UNDERTAKE TECHNOLOGY BASED BUSINESS TRANSFER ALLOWING REORGANIZATION IN TIME WITH TODAY'S DYNAMIC DIGITAL BUSINESS ENVIRONMENT.
                    </p>
                    <div>
                      <button class="btn btn-primary">More About Rozecart</button>
                    </div>
                </div>
              </div>
              <br>
              <div class="col-md-5">
                <div class="container-fluid">
                  <img src="./image/about-banner.jpg" class="img-responsive">
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    <br><br><hr>
      <!--     services of the rozecart -->
    <div class="container-fluid section1" id="service">
      <div class="container-fluid">
        <h3 class="team">Service</h3>
          <span class="line"></span>
              <br><hr>
        <div class="container-fluid  parallax"> 
          <section  class="container content-section text-center ">
            <div class="row">
              <div class="col-lg-12 col-lg-offset-0">
                <div class="row">
                  <div class="col-md-3 col-md-offset-1 animated pulse block hvr-rectangle-out  content-section text-center ">
                        <a href=""><i class="fa fa-check-square-o" style="font-size: 50px; color: #fb5f2f; margin-top: 15%;height:100px;" aria-hidden="true"></i></a>  <span class="line" > </span>
                      <h4>EASY TO USE</h4>
                  </div>
                  <div class="col-md-3 col-md-offset-1 animated pulse block hvr-rectangle-out">
                        <a href=""><i class="fa fa-cog " style="font-size: 50px; color: #fb5f2f; margin-top: 15%;height:100px;" aria-hidden="true"></i></a> <span class="line" > </span>
                      <h4> HIGHLY CUSTOMIZABLE</h4> 
                  </div>
                  <div class="col-md-3 col-md-offset-1 animated pulse block hvr-rectangle-out">
                        <a href=""><i class="fa fa-laptop " style="font-size:50px; color: #fb5f2f; margin-top: 15%;height:100px;" aria-hidden="true"></i></a> <span class="line" > </span>
                      <h4> FULLY RESPONSIVE</h4> 
                  </div>
                </div>
                  <br><br><br>
                <div class="row">
                  <div class="col-md-3 col-md-offset-1 animated pulse block hvr-rectangle-out">
                  <a href=""> <i class="fa fa-shopping-cart " style="font-size:50px; color: #fb5f2f; margin-top: 15%;height:100px;" aria-hidden="true"></i></a> <span class="line" > </span>
                      <h4> EASY SHOPPING</h4> 
                  </div>
                  <div class="col-md-3 col-md-offset-1 animated pulse block hvr-rectangle-out">
                  <a href=""> <i class="fa fa-truck " style="font-size:50px; color: #fb5f2f; margin-top: 15%;height:100px;" aria-hidden="true"></i></a> <span class="line" > </span>
                      <h4> FAST DELIVERY</h4> 
                  </div>
                  <div class="col-md-3 col-md-offset-1 animated pulse block hvr-rectangle-out">
                  <a href=""> <i class="fa fa-money " style="font-size:50px; color: #fb5f2f; margin-top: 15%;height:100px;" aria-hidden="true"></i></a> <span class="line" > </span>
                      <h4>RELIABLE PRICE</h4> 
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    <br>
    <hr>
    <!-- our great team of rozecart -->
     <section class="" id="team">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Our Amazing Team</h2>
             <span class="line"> </span>
            <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="team-member content-section text-center">
              <img class="mx-auto rounded" src="./image/rupesh.jpg"   width="50%" alt="">
              <h4>Rupesh Pandey</h4>
              <p class="text-muted">Lead Designer</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-4 bottom">
            <div class="team-member content-section text-center">
              <img class="mx-auto rounded-circle" src="./image/rupesh.jpg" width="50%" alt="">
              <div class="">
              <h4>Rupesh Pandey</h4>
              <p class="text-muted">Lead Marketer</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-linkedin"></i>
                  </a>
                </li>
              </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="team-member content-section text-center">
              <img class="mx-auto rounded-circle" src="./image/rupesh.jpg" width="50%" alt="">
              <h4>Rupesh Pandey</h4>
              <p class="text-muted">Lead Developer</p>
              <ul class="list-inline social-buttons">
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-twitter"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-facebook"></i>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a href="#">
                    <i class="fa fa-linkedin"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
  <!--   <div class="container-fluid">
      <div class="row" id="team">
        <h3 class="team">Team</h3>
          <span class="line"> </span>
            <br><hr>
        <div class="col-md-3 col-md-offset-1 member">
          <div class="container-fluid  content-section text-center member-photo">
            <img src="./image/rupesh.jpg" style="position: center; max-width: 40%;">
          </div>
          <div class="container-fluid  content-section text-center member-data">
            <h3>Rupesh Pandey</h3>
            <h5>CEO & Founder</h5>
            <a href="#"><i class="fa fa-dribbble"></i></a> 
            <a href="#"><i class="fa fa-twitter"></i></a> 
            <a href="#"><i class="fa fa-linkedin"></i></a> 
            <a href="#"><i class="fa fa-facebook"></i></a> 
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1 ">
          <div class="container-fluid  content-section text-center">
            <img src="./image/rupesh.jpg" style="max-width: 50%; position: center;">
          </div>
          <div class="container-fluid  content-section text-center">
            <h3>Rupesh Pandey</h3>
            <h5>CEO & Founder</h5>
            <a href="#"><i class="fa fa-dribbble"></i></a> 
            <a href="#"><i class="fa fa-twitter"></i></a> 
            <a href="#"><i class="fa fa-linkedin"></i></a> 
            <a href="#"><i class="fa fa-facebook"></i></a> 
          </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
          <div class="container-fluid  content-section text-center">
            <img src="./image/rupesh.jpg" style="max-width: 40%; position: center;">
          </div>
          <div class="container-fluid  content-section text-center">
            <h3>Rupesh Pandey</h3>
            <h5>CEO & Founder</h5>
            <a href="#"><i class="fa fa-dribbble"></i></a> 
            <a href="#"><i class="fa fa-twitter"></i></a> 
            <a href="#"><i class="fa fa-linkedin"></i></a> 
            <a href="#"><i class="fa fa-facebook"></i></a> 
          </div>
        </div>
      </div>
    </div> -->
      <br><hr>
      <!-- how we do our work -->
    <div class="container-fluid">    
      <div class="container-fluid" id="work">
        <div class="row" >
            <h3 class="work">Our Work</h3>
              <span class="line"></span>
                        <br><hr>
            <div class="col-md-4 col-md-offset-1">
              <div class="container-fluid">
                <p style="line-height: 40px;" class="text3">
                  Rozecart works with clients across the country that uses operating and infrastructure systems. Our clients span from small to medium size businesses to fortune 100 customers! Our capabilities and skill sets range from simple service and trouble tickets to full multisite deployments.
                  Rozecart works closely with their clients to make important decisions on existing platforms. We pursue the maximum usage of existing hardware in order to save the client money. As a client looks to upgrade their network, we will strategize to ensure the smoothest transition, timely installs, and work to ensure all costs stay at a minimum.
                </p>
              </div>  
            </div>
            <div class="col-md-5 col-md-offset-1">
              <div class="container-fluid">
                <img class="img-responsive" src="./image/rawpixel-com-247360.jpg"  width="150%" height="150%">
              </div>
            </div>
        </div>
      </div>
    </div>   
      <br><hr>
    <!--  footer  section -->
    <div id="contact">
      <div class="footer" >
        <div class="footerbody">
          <div class="container-fluid">
            <div class="contact col-md-12">
              <h3 class="foothead">Contact</h3>
                <span class="line"></span>
                <hr>
              <h4 class="text1">Donec nec justo eget felis facilisis fermentum. Aliquam dignissim felis auctor ultrices ut elementum.</h4>
                <br>
              <div class="text2">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi volutpat commodo enim aliquet laoreet. Mauris vel egestas erat, vitae feugiat ligula. Cras quis commodo ex, consequat consectetur quam. Etiam accumsan in nunc vitae bibendum. Curabitur dignissim non nisi quis vestibulum. Quisque porta ipsum vitae metus tempus, vel convallis dui efficitur. Praesent vitae mattis augue. Curabitur ornare mollis mi, at laoreet leo tempor id. Nam ornare erat vel augue tempor pretium. Morbi porta pellentesque dolor ullamcorper viverra. Phasellus mattis ipsum eget ipsum cursus, id interdum nibh laoreet.
                </p>
              </div>
                <br><br>
              <div class="row content-section text-center">
                <div class="col-md-2 col-md-offset-1 font"><i class="fa fa-phone fa-4x" aria-hidden="true"></i>
                  <div class="content-section text-center">
                   <span>+91 120 475 0211</span>
                  </div>
                </div>
                    
                <div class="col-md-2 col-md-offset-1 font content-section text-center"><i class="fa fa-home fa-4x" aria-hidden="true"></i><p> 2nd Floor, C-54 Sector 2, Noida, UP - 201301</p>
                </div>
                <div class="col-md-2 col-md-offset-1 font content-section text-center"><i class="fa fa-envelope fa-4x" aria-hidden="true"></i><p>  admin@cyberoze.com</p>
                </div>
              </div>
            </div>  
          </div>
          <div class="container-fluid">
            <div class="form col-md-8 col-md-offset-2">
              <form class="form-horizontal">
                  <div class="form-group">
                    <label for="name" class="footmenu">Name:</label>
                    <input type="name" class="form-control" id="name">
                  </div>
                  <div class="form-group">
                    <label for="email" class="footmenu">Email address:</label>
                    <input type="email" class="form-control" id="email">
                  </div>
                  <div class="form-group">
                    <label for="text" class="footmenu">Subject:</label>
                    <input type="subject" class="form-control" id="subject">
                  </div>
                  <div class="form-group">
                    <label for="comment" class="footmenu">Feedback:</label>
                    <textarea class="form-control" rows="5" id="comment"></textarea>
                  </div>
                  <button type="submit" class="btn btn-default">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12 footerbar">
              <p>&copy;2015- <?php echo date("y"); ?> Rozecart All Right Reserved</p>
          </div>
    </div>

      <!--     javascript code of rozecart -->
  <script type="text/javascript">
      $(function(){
        $(window).scroll(function(){
          if($(window).scrollTop()>= 100)
            {
              $('nav').addClass('scrolled');
            }
          else
            {
              $('nav').removeClass('scrolled');
            }
        });
        $('a.page-scroll').click(function()
         {
            $('html, body').animate
            ({
              scrollTop: $( $(this).attr('href') ).offset().top
              }, 2000);
            return false;
         });
         $("#roze").mouseenter(function(){
          $(this).css("color"," #fb5f2f");
           $(this).addClass("messi");
         }) ;
                  $("#roze").mouseleave(function(){
          $(this).css("color","white");
          $(this).removeClass("messi");

         }) ;
                  $(".lists").mouseenter(function(){
          $(this).css("color"," #fb5f2f");
          $(this).addClass("messi");
         }) ;
                  $(".lists").mouseleave(function(){
          $(this).css("color","white");
      $(this).removeClass("messi");
         }) ;


      });
    
         
      
    </script>
    <script type="text/javascript">
      $(function(){
$(".block").hover(){

}

      });
    </script>
  </body>
</html>
